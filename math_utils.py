import bpy
from mathutils import Vector
from mathutils.geometry import intersect_point_line






# check if point is closer to bone_1 or to bone_2
def check_distance(bone_1,bone_2,point) :
    if bone_2 :

        distance = {}

        distance[(point-bone_1['tail']).length] = True
        distance[(point-bone_1['head']).length] = True

        distance[(point-bone_2['tail']).length]  = False
        distance[(point-bone_2['head']).length]  = False

        minimum = min(distance.keys())

        return(distance[minimum])

    else :
        return True


def round_vector(vector) :
    x = round(vector[0],5)
    y = round(vector[1],5)
    z = round(vector[2],5)

    return Vector((x,y,z))

"""
def intersect(pt,ref_bone) :
    intersection = intersect_point_line(pt,ref_bone['tail'],ref_bone['head'])


    distance =  (intersection[0]-pt).length


    if distance < 0.001 :
        return True
    else :
        return False
"""

def intersect(pt, bone) :
    intersection = intersect_point_line(pt,bone['head'],bone['tail'])

    point_on_line = (pt-intersection[0]).length < 0.00001

    #print('point_on_line',point_on_line)
    #distance = True
    is_in_range = False
    if intersection[1]<=0.5 :
        distance = (pt-bone['head']).length
        #print(distance)
        if intersection[1]>=0 :
            is_in_range = True
        else :
            is_in_range = distance > -1.5

    elif intersection[1]>0.5 :
        distance = (pt-bone['tail']).length

        #print(distance)
        if intersection[1]<=1 :
            is_in_range = True
        else :
            is_in_range = distance < 2.5

    #print('is_in_range',is_in_range)
    #print(bone)
    #print('intersection',intersection)
    #print('point_on_line',point_on_line)

    #print('is_in_range',is_in_range)


    if point_on_line and is_in_range:
        return intersection[1]




