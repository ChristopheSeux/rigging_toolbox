import bpy
from rigging_toolbox.rig_utils import armature_info
from rigging_toolbox.math_utils import intersect

import bpy
from mathutils.geometry import intersect_point_line
from mathutils import Matrix, Vector
from math import pi


##b = bone, rb = ref_bone
def find_ref_roll(metarig,b) :
    distance = {}
    for bone_name,rb in metarig.items() :

        tail_distance = min((rb['tail']-b['tail']).length,(rb['tail']-b['head']).length)
        head_distance = min((rb['head']-b['tail']).length,(rb['head']-b['head']).length)



        distance[tail_distance+head_distance] = bone_name

    #print(tail_distance,head_distance)

    minimum = min(distance.keys())

    #print(distance[minimum])
    return(distance[minimum])


def inside(point,bone) :
    a = (point-bone['tail']).length
    b = (point-bone['head']).length

    #print(a+b-bone.length)

    if not a+b - bone['length'] <0.001 and  b<a:
        return False
    else :
        return True


def bind_bones(rig, metarig):
    """Write custom properties on each bone relative to the metarig"""

    metarig_values = armature_info(metarig)
    rig_values = armature_info(rig)

    for bone_name, bone in rig_values.items() :
        match = {}

        for ref_bone_name, ref_bone in metarig_values.items() :

            head_intersection = intersect(bone['head'], ref_bone)
            tail_intersection = intersect(bone['tail'], ref_bone)

            if head_intersection is not None :
                if not 'head' in match or 'head' in match and abs(head_intersection) < abs(match['head']) :
                    match['ref_head'] = ref_bone_name
                    match['head'] = head_intersection

            if tail_intersection is not None :
                if not 'tail' in match or 'tail' in match and abs(tail_intersection) < abs(match['tail']) :
                    match['ref_tail'] = ref_bone_name
                    match['tail'] = tail_intersection


        if "tail" in match and "head" in match:
            if match['ref_tail'] == match['ref_head'] :
                roll_bone_name = match['ref_tail']

            else :
                roll_bone_name = find_ref_roll(metarig_values,bone)

            ref_roll_bone = metarig_values[roll_bone_name]

            roll = (ref_roll_bone['roll']- bone['roll']) #% pi

            match['roll'] =  roll
            match['ref_roll'] = roll_bone_name

        rig.data.bones[bone_name]['match'] = match

def match_bones(rig, metarig):
    """Move the bone to match the metarig"""

    metarig_values = armature_info(metarig)
    rig_values = armature_info(rig)

    ##### EDIT BONES
    active_object = bpy.context.view_layer.objects.active
    mode = active_object.mode
    bpy.ops.object.mode_set(mode= 'OBJECT')
    bpy.context.view_layer.objects.active = rig
    bpy.ops.object.mode_set(mode= 'EDIT')
    #####

    for bone in rig.data.edit_bones :

        print('bone', bone.name)

        if bone.get('match') :

            match = bone["match"]

            ref_tail = metarig_values.get(match['ref_tail']) if match.get('ref_tail') else None
            ref_head = metarig_values.get(match['ref_head']) if match.get('ref_head') else None
            ref_roll = metarig_values.get(match['ref_roll']) if match.get('ref_roll') else None


            if ref_tail and ref_head :
                #bpy.context.scene.cursor_location = ref_tail.head_local
                #bone.tail = ref_tail.vector * match["tail"]+ref_tail.head_local
                #bone.head = ref_head.vector* match["head"]+ref_head.head_local
                bone.head = ref_head["head"] + ref_head["vector"]*match["head"]
                bone.tail = ref_tail["head"] + ref_tail["vector"]*match["tail"]

            elif ref_tail and not ref_head :


                bone.head = ref_tail["vector"] * match["tail"]+ref_tail['head'] - bone.vector

                bone.tail = ref_tail["vector"]  * match["tail"]+ref_tail['head']
                #bone.head = ref_tail.vector * match["tail"]+bone.head


            elif ref_head and not ref_tail:

                bone.tail = ref_head["vector"] * match["head"]+ref_head["head"] + bone.vector
                bone.head = ref_head["head"]+ ref_head["vector"]*match["head"]



            if ref_roll :
                bone.roll = ref_roll['roll'] - match["roll"]

    bpy.ops.object.mode_set(mode= 'OBJECT')
    bpy.context.view_layer.objects.active = active_object
    bpy.ops.object.mode_set(mode= mode)

    #bpy.ops.object.mode_set(mode= 'OBJECT')
    #bpy.context.scene.objects.active = ob

    #reset_bone_stretch(rig)

def update_rig(rig,autorig,metarig):
    scene = bpy.context.scene
    ob = bpy.context.object

    autorig = scene.BoneMatch.autorig

    autorig_copy = autorig.copy()
    autorig_copy.data = autorig.data.copy()

    action = ob.animation_data.action
    name = ob.name

    ob.user_remap(autorig_copy)

    scene.objects.active = autorig_copy

    autorig.user_remap(autorig_copy)

    bpy.ops.bonematch.match_bones()

    if action :
        autorig_copy.animation_data.action = action

    autorig.name = name
    autorig.data.name = name
    #users = find_rig_users(ob)

def old_update_rig(rig,autorig,metarig):
    scene = bpy.context.scene
    #metarig_values = armature_info(metarig)
    rig_values = armature_info(rig)
    active_object = bpy.context.scene.objects.active
    mode = active_object.mode

    data_attr = ['bbone_curveinx','bbone_curveiny','bbone_curveoutx','bbone_curveouty','bbone_in',
    'bbone_out','bbone_rollin','bbone_rollout','bbone_scalein','bbone_scaleout','bbone_segments','bbone_x',
    'bbone_z','show_wire'
    ]

    pose_attr =['custom_shape','custom_shape_scale']



    #ob = bpy.data.objects.new('autorig', autorig.copy())
    ob = autorig.copy()
    ob.name = 'autorig'
    ob.data = autorig.data.copy()

    autorig.user_remap(ob)

    scene.objects.link(ob)
    ob.select = True
    scene.objects.active = ob

    #match with the metarig
    match_bones(ob,metarig)


    extra_bones = [b.name for b in rig.data.bones if b.name not in [b.name for b in autorig.data.bones]]

    bpy.ops.object.mode_set(mode= 'EDIT')

    # add extra bones
    for ref_bone in extra_bones :
        edit_bone = ob.data.edit_bones.new(ref_bone)
        pose_bone = rig.pose.bones.get(ref_bone)
        print("####",edit_bone,pose_bone)
        info  = rig_values.get(ref_bone)

        edit_bone.tail = info['tail']
        edit_bone.head = info['head']
        edit_bone.roll = info['roll']

        edit_bone.parent = ob.data.edit_bones.get(pose_bone.parent.name)


    bpy.ops.object.mode_set(mode= 'OBJECT')

    #constraints
    for ref_bone in extra_bones :
        pose_bone = ob.pose.bones.get(ref_bone)
        ref_pose_bone = rig.pose.bones.get(ref_bone)

        for ref_constraint in ref_pose_bone.constraints :
            c = pose_bone.constraints.new(type = ref_constraint.type)

            for attr in [p.identifier for p in ref_constraint.bl_rna.properties] :
                value = getattr(ref_constraint,attr)
                try :
                    setattr(c,attr,value)
                except :
                    print(attr)

    rig.user_remap(ob)
    #update bone data attributes
    for pbone in ob.pose.bones :
        ref_bone = rig.pose.bones.get(pbone.name)
        if ref_bone :
            for attr in data_attr :
                value = getattr(ref_bone.bone,attr)
                setattr(pbone.bone,attr,value)

            for attr in pose_attr :
                value = getattr(ref_bone,attr)
                setattr(pbone,attr,value)
    #pose attr

    #bpy.data.objects.remove(ob,True)

    bpy.context.scene.objects.active = active_object
    bpy.ops.object.mode_set(mode= mode)