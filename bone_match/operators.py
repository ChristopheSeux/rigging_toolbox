import bpy

from rigging_toolbox.bone_match.functions import (bind_bones, match_bones)

from rigging_toolbox.rig_utils import (get_selected_bones, set_pole_angle, 
    reset_bone_stretch, get_bone_datas, new_bones_from_data)#, update_rig

from bpy.props import FloatProperty, BoolProperty, FloatVectorProperty
import json



class BONEMATCH_OT_copy_bones_to_metarig(bpy.types.Operator):
    """Copy bones to metarig"""
    bl_idname = "bonematch.copy_bones_to_metarig"
    bl_label = "Copy bones to metarig"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        ob = context.object
        return ob and ob.type == 'ARMATURE' and ob.data.metarig

    def execute(self, context):
        rig = context.object
        metarig = rig.data.metarig

        bones = get_selected_bones(rig, mode='EDIT')
        bone_datas = get_bone_datas(bones)
        new_bones_from_data(metarig, bone_datas)

        return {'FINISHED'}


class BONEMATCH_OT_bind_bones(bpy.types.Operator):
    """Write custom properties on each bone relative to the metarig"""
    bl_idname = "bonematch.bind_bones"
    bl_label = "Bind bones to metarig"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return context.object and context.object.type == 'ARMATURE'

    def execute(self, context):
        rig = context.object
        bind_bones(rig, rig.data.metarig)

        return {'FINISHED'}


class BONEMATCH_OT_match_bones(bpy.types.Operator):
    """Move the bone to match the metarig"""
    bl_idname = "bonematch.match_bones"
    bl_label = "Match bones to metarig"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return context.object and context.object.type == 'ARMATURE'

    def execute(self, context):
        rig = context.object

        match_bones(rig, rig.data.metarig)
        reset_bone_stretch(rig)

        #for bone in rig.pose.bones:
        #    set_pole_angle(bone)

        return {'FINISHED'}

'''
class BONEMATCH_OT_update_rig(bpy.types.Operator):
    bl_idname = "rigtb.update_rig"
    bl_label = "Update rig "
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return context.object and context.object.type == 'ARMATURE'

    def execute(self, context):
        # rig = context.object
        # autorig = context.scene.BoneMatch.autorig
        # metarig = context.scene.BoneMatch.metarig

        # update_rig(rig, autorig, metarig)

        return {'FINISHED'}
'''

classes = (
    BONEMATCH_OT_copy_bones_to_metarig,
    BONEMATCH_OT_bind_bones,
    BONEMATCH_OT_match_bones
)

def register():
    for c in classes:
        bpy.utils.register_class(c)

def unregister():
    for c in reversed(classes) :
        bpy.utils.unregister_class(c)
