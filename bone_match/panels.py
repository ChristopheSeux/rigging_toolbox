import bpy

class RIGTB_PT_bone_match(bpy.types.Panel):
    bl_label = "Bone Match"
    bl_category = "Rigging"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    # bl_context = "posemode"

    @classmethod
    def poll(cls, context):
        return context.object and context.object.type == 'ARMATURE'

    def draw(self, context):
        layout = self.layout
        row = layout.row(align=True)
        row.prop(context.object.data, 'metarig', text='Metarig')
        row = layout.row(align=True)
        row.operator("bonematch.bind_bones", icon="ARMATURE_DATA", text='Bind' )
        row.operator("bonematch.match_bones", icon="MOD_ARMATURE", text='Match')
        layout.separator()
        #row = layout.row(align=True)

        #row.operator("bonematch.reset_bone_stretch", icon="CONSTRAINT_BONE", text='Reset Bone Stretch')

        row = layout.row(align=True)
        row.operator("bonematch.copy_bones_to_metarig", text='Copy Bones to Metarig', icon='BONE_DATA')
        #row = layout.row(align=True)
        #row.operator("rigtb.set_ik_pole_angles", text='Set ik pole angles', icon='CONSTRAINT_BONE')
        #row = layout.row(align=True)
        #row.operator("rigtb.clear_invalid_drivers", text='Clear Unvalid Drivers', icon='DRIVER')

        #row.operator("object.update_rig", icon="FILE_REFRESH", text='Update')

classes = (
    RIGTB_PT_bone_match,
)

def register():
    for c in classes:
        bpy.utils.register_class(c)

def unregister():
    for c in reversed(classes) :
        bpy.utils.unregister_class(c)