from rigging_toolbox.bone_match import functions
from rigging_toolbox.bone_match import operators
from rigging_toolbox.bone_match import panels

if "bpy" in locals():
    import importlib
    importlib.reload(functions)
    importlib.reload(operators)
    importlib.reload(panels)

import bpy

def register():
    operators.register()
    panels.register()

    bpy.types.Armature.metarig = bpy.props.PointerProperty(
        type=bpy.types.Object, 
        poll=lambda s, o: o.type=='ARMATURE' and o.data !=s
    )

def unregister():
    del bpy.types.Armature.metarig
    panels.unregister()
    operators.unregister()
