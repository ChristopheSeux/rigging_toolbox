
import bpy

import bpy
import bmesh
import json
from bpy.props import StringProperty, EnumProperty
import re

from rigging_toolbox.rig_utils import (symmetrize_driver, is_left_side, reset_bone_stretch, 
    get_selected_bones, get_bone_datas, new_bones_from_data, set_pole_angle, split_path,
    get_bones_by_side)



class RIGTB_OT_symmetrize_bones(bpy.types.Operator):
    """Symmetrize Bones"""
    bl_idname = "rigtb.symmetrize_bones"
    bl_label = "Symmetrize Drivers"
    bl_options = {'REGISTER', 'UNDO'}

    direction : EnumProperty(items=[(m.upper(), m, '') for m in ('+X to -X', '-X to +X')], default='+X TO -X')

    @classmethod
    def poll(cls, context):
        return context.object and context.object.type == 'ARMATURE'

    def execute(self, context):
        rig = context.object
        mode = rig.mode

        layers = list(rig.data.layers)
        bone_groups = {b.name: b.bone_group for b in rig.pose.bones}
        bone_shapes = {b.name: b.custom_shape for b in rig.pose.bones}
        hide_selects = {b.name: b.bone.hide_select for b in rig.pose.bones}

        if self.direction == '-X TO +X':
            side = 'LEFT'
            symmetrize_direction = "NEGATIVE_X"
        else:
            symmetrize_direction = "POSITIVE_X"
            side = 'RIGHT'

        rig.data.layers = [True] * 32
        bpy.ops.object.mode_set(mode='EDIT')       

        bones = get_bones_by_side(rig, side)
        bone_names = {b.name for b in bones}
        for bone in list(rig.data.edit_bones):
            bone.hide_select = False
            if bone.name in bone_names:
                #print(f'Remove bone {bone.name}')
                rig.data.edit_bones.remove(bone)

        bpy.ops.armature.reveal()
        bpy.ops.armature.select_all(action='SELECT')
        #raise Exception()
        bpy.ops.armature.symmetrize(direction=symmetrize_direction)
        #fcurves = [fc for fc in ob.animation_data.drivers if is_left_side(fc.data_path)]
        
        bpy.ops.object.mode_set(mode='OBJECT')

        rig.data.layers = layers

        # Reassign bone_group
        for bone in rig.pose.bones:
            if bone.name in bone_groups:
                bone.bone_group = bone_groups[bone.name]
            if bone.name in bone_shapes:
                bone.custom_shape = bone_shapes[bone.name]

        bpy.ops.rigtb.symmetrize_drivers(direction=self.direction)
        bpy.ops.object.mode_set(mode=mode)

        
        return {'FINISHED'}


class RIGTB_OT_symmetrize_drivers(bpy.types.Operator):
    """Symmetrize Drivers"""
    bl_idname = "rigtb.symmetrize_drivers"
    bl_label = "Symmetrize Drivers"
    bl_options = {'REGISTER', 'UNDO'}

    direction : EnumProperty(items=[(m.upper(), m, '') for m in ('+X to -X', '-X to +X')], default='+X TO -X')

    @classmethod
    def poll(cls, context):
        return context.object

    def execute(self, context):
        ob = context.object

        if not ob.animation_data:
            return {'CANCELLED'}

        if self.direction == '-X TO +X':
            fcurves = [fc for fc in ob.animation_data.drivers if is_left_side(fc.data_path) is False]
        
        elif self.direction == '+X TO -X':
            fcurves = [fc for fc in ob.animation_data.drivers if is_left_side(fc.data_path)]

        print(f'Symmetrizing {len(fcurves)} Drivers')
        for fcurve in fcurves:
            symmetrize_driver(fcurve)

        return {'FINISHED'}


class RIGTB_OT_mix_weight_from_clipboard(bpy.types.Operator):
    """Copy Weight to Clipboard"""
    bl_idname = "rigtb.mix_weight_from_clipboard"
    bl_label = "Mix Weight from Clipboard"
    bl_options = {'REGISTER', 'UNDO'}

    mode : EnumProperty(items=[(m, m.title(), '') for m in ('SUBSTRACT', 'ADD', 'MULTIPLY', 'MULTIPLY_BY_INVERSE')], default='ADD')

    @classmethod
    def poll(cls, context):
        ob = context.object
        return ob and ob.type == 'MESH'

    def execute(self, context):

        ob = bpy.context.object
        if ob.mode == 'EDIT':
            bm = bmesh.from_edit_mesh(ob.data)
        else:
            bm = bmesh.new()
            bm.from_mesh(ob.data)

        #bm.from_mesh(ob.data, face_normals=False, vertex_normals=False)
        layer = bm.verts.layers.deform.active
        vg_index = ob.vertex_groups.active_index

        vertex_groups_data = json.loads(bpy.context.window_manager.clipboard)

        bm.verts.ensure_lookup_table()
        for vert_index, weight in vertex_groups_data.items():
            
            vert = bm.verts[int(vert_index)]
            
            current_weight = 0
            if layer:
                current_weight = vert[layer].get(vg_index, 0)
            
            if self.mode == 'ADD':
                new_weight = current_weight + weight
            elif self.mode == 'SUBSTRACT':
                new_weight = current_weight - weight
            elif self.mode == 'MULTIPLY':
                new_weight = current_weight * weight
            elif self.mode == 'MULTIPLY_BY_INVERSE':
                new_weight = current_weight * (1 - weight)

            vert[layer][vg_index] = new_weight
        
        if ob.mode != 'EDIT':
            bm.to_mesh(ob.data)

        ob.data.update()

        mode = self.mode.replace('_', ' ').title()
        self.report({"INFO"}, f'The Clipboard was mixed with mode: {mode} to the active weight: {ob.vertex_groups.active.name}')


        return {'FINISHED'}

    def draw(self, context):
        self.layout.use_property_split = True

        row = self.layout.row()
        row.prop(self, "mode")


class RIGTB_OT_copy_weight_to_clipboard(bpy.types.Operator):
    """Copy Weight to Clipboard"""
    bl_idname = "rigtb.copy_weight_to_clipboard"
    bl_label = "Copy Weight to Clipboard"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        ob = context.object
        return ob and ob.type == 'MESH' and ob.vertex_groups.active

    def execute(self, context):
        ob = bpy.context.object

        is_selection = False
        if ob.mode == 'EDIT':
            bm = bmesh.from_edit_mesh(ob.data)
            is_selection = any(v.select for v in bm.verts)
        else:
            bm = bmesh.new()
            bm.from_mesh(ob.data)

        #bm.from_mesh(ob.data, face_normals=False, vertex_normals=False)
        layer = bm.verts.layers.deform.active
        vg_index = ob.vertex_groups.active_index

        vertex_groups_data = {}

        for vert in bm.verts:
            if not vert.select and is_selection:
                continue
            
            weight = 0
            if layer:
                weight = vert[layer].get(vg_index, 0)
            
            vertex_groups_data[vert.index] = weight
        

        bpy.context.window_manager.clipboard = json.dumps(vertex_groups_data)

        self.report({"INFO"}, f'Weight: {ob.vertex_groups.active.name} was copied to clipboard')

        #bm.free() 

        return {'FINISHED'}


class RIGTB_OT_reset_stretch(bpy.types.Operator):
    """Reset rest length of bones with stretch constaint"""
    bl_idname = "rigtb.reset_bone_stretch"
    bl_label = "Reset length in stretch constraint"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return context.object and context.object.type == 'ARMATURE'

    def execute(self, context):
        rig = context.object
        reset_bone_stretch(rig)

        return {'FINISHED'}


class RIGTB_OT_copy_bones_to_clipboard(bpy.types.Operator):
    """Copy bones to clipboard"""
    bl_idname = "rigtb.copy_bones_to_clipboard"
    bl_label = "Copy bones to clipboard"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        ob = context.object
        return ob and ob.type == 'ARMATURE'

    def execute(self, context):
        rig = context.object

        bones = get_selected_bones(rig, mode='EDIT')
        bone_datas = get_bone_datas(bones)
        context.window_manager.clipboard = json.dumps(bone_datas)

        return {'FINISHED'}


class RIGTB_OT_paste_bones_from_clipboard(bpy.types.Operator):
    """Paste Bones from Clipboard"""
    bl_idname = "rigtb.paste_bones_from_clipboard"
    bl_label = "Paste Bones from Clipboard"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        ob = context.object
        return ob and ob.type == 'ARMATURE'

    def execute(self, context):
        rig = context.object

        bone_datas = json.loads(context.window_manager.clipboard)
        new_bones_from_data(rig, bone_datas)

        return {'FINISHED'}


class RIGTB_OT_set_deform_bones(bpy.types.Operator):
    """Set Deform on Bones starting with DEF-"""
    bl_idname = "rigtb.set_deform_bones"
    bl_label = "Set Deform Bones"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        ob = context.object
        return ob and ob.type == 'ARMATURE'

    def execute(self, context):
        rig = context.object


        for bone in rig.data.bones:
            if re.match(r'DEF[-_.]', bone.name):
                bone.use_deform = True
            else:
                bone.use_deform = False

        return {'FINISHED'}


class RIGTB_OT_set_bones_euler(bpy.types.Operator):
    """Set Euler on Bones starting with prefix"""
    bl_idname = "rigtb.set_bones_euler"
    bl_label = "Set Euler Bones"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        ob = context.object
        return ob and ob.type == 'ARMATURE'

    def execute(self, context):
        rig = context.object

        for bone in rig.pose.bones:
            if bone.rotation_mode != 'QUATERNION':
                continue

            if not re.match(r'[A-Z]+[-_.]', bone.name):
                bone.rotation_mode = 'XYZ'

        return {'FINISHED'}


class RIGTB_OT_clear_invalid_drivers(bpy.types.Operator):
    """Clear Unvalid Drivers"""
    bl_idname = "rigtb.clear_invalid_drivers"
    bl_label = "Clear Unvalid Drivers"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        ob = context.object
        return ob and ob.type == 'ARMATURE'

    def execute(self, context):
        rig = context.object
        for fcurve in rig.animation_data.drivers:

            try:
                rig.path_resolve(fcurve.data_path)
            except ValueError:
                rig.animation_data.drivers.remove(fcurve)


        return {'FINISHED'}


class RIGTB_OT_set_ik_pole_angles(bpy.types.Operator):
    """Set IK Pole angle of bones"""
    bl_idname = "rigtb.set_ik_pole_angles"
    bl_label = "Set ik pole angles"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        ob = context.object
        return ob and ob.type == 'ARMATURE'

    def execute(self, context):
        rig = context.object
        bones = get_selected_bones(rig, mode='POSE')
        if not bones:
            bones = rig.pose.bones

        for bone in bones:
            set_pole_angle(bone)

        return {'FINISHED'}


class RIGTB_OT_rename_bone(bpy.types.Operator):
    """Rename a bone and rempap drivers"""
    bl_idname = "rigtb.rename_bone"
    bl_label = "Rename bone"
    bl_options = {'REGISTER', 'UNDO'}

    bone_name : StringProperty(name='Name')

    @classmethod
    def poll(cls, context):
        return context.active_bone

    def invoke(self, context, event):
        self.bone_name = context.active_bone.name
        return context.window_manager.invoke_props_dialog(self)

    def execute(self, context):
        wm = context.window_manager
        rig = context.object
        bone = context.active_bone

        old_name = bone.name
        new_name = self.bone_name

        for fc in rig.animation_data.drivers:
            for var in fc.driver.variables:
                if var.type == 'SINGLE_PROP':
                    target = var.targets[0]
                    old_data_path = target.data_path
                    bone_path = f'pose.bones["{old_name}"]'

                    if bone_path in target.data_path:
                        target.data_path = target.data_path.replace(bone_path, f'pose.bones["{new_name}"]')
                        print(f'Data path remaped from {old_data_path} to {target.data_path}')

        bone.name = new_name

        return {'FINISHED'}


class RIGTB_OT_hide_bones(bpy.types.Operator):
    """Hide bones left or right"""
    bl_idname = "rigtb.hide_bones"
    bl_label = "Rename bone"
    bl_options = {'REGISTER', 'UNDO'}

    side : EnumProperty(items=[(m.upper(), m, '') for m in ('Left', 'Right', 'Center')], default='RIGHT')

    @classmethod
    def poll(cls, context):
        return context.object and context.object.type == 'ARMATURE'

    def execute(self, context):
        wm = context.window_manager
        rig = context.object

        bones = get_bones_by_side(rig, self.side)
        for bone in rig.data.bones:
            if bone in bones:
                bone.hide = True

        return {'FINISHED'}


class RIGTB_OT_isolate_bone_hierarchy(bpy.types.Operator):
    """Isolate Bone Hierarchy"""
    bl_idname = "rigtb.isolate_bone_hierarchy"
    bl_label = "Isolate Bone Hierarchy"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return context.active_bone

    def execute(self, context):
        wm = context.window_manager
        rig = context.object

        hierarchy = [c for b in context.selected_pose_bones for c in (*b.parent_recursive, b)]
        #hierarchy += [b.name for b in context.selected_pose_bones for c in b.parent_recursive]

        for pose_bone in rig.pose.bones:
            pose_bone.bone.hide = pose_bone not in hierarchy

        for pose_bone in hierarchy:
            # Show bone that are related the hierarchy by constraint
            for constraint in pose_bone.constraints:
                for prop_name in constraint.bl_rna.properties.keys():
                    if not 'subtarget' in prop_name:
                        continue
                    
                    target_prop_name = prop_name.replace('subtarget', 'target')
                    if not hasattr(constraint, target_prop_name):
                        target_prop_name = prop_name.replace('subtarget', 'object')

                    #print(target_ob, prop_name)
                    target_ob = getattr(constraint, target_prop_name)
                    if not target_ob or target_ob.type !='ARMATURE':
                        continue
                    
                    target_bone = target_ob.pose.bones.get(getattr(constraint, prop_name))

                    if target_bone:
                        target_bone.bone.hide = False
            
            # Show bone that are related the hierarchy by driver
            if not rig.animation_data:
                return {'FINISHED'}
 
            fcurves = [fc for fc in rig.animation_data.drivers if f'pose.bones["{pose_bone.name}"]' in fc.data_path]
            for fcurve in fcurves:
                for variable in fcurve.driver.variables:
                    if variable.type == 'SINGLE_PROP':
                        target = variable.targets[0]
                        splitted_path = split_path(target.data_path)
                        if 'pose.bones' not in target.data_path or not target.id or not splitted_path:
                            continue

                            bone_name = splitted_path[0]
                            target_bone = target.id.pose.bones.get(bone_name)
                            
                            if target_bone:
                                target_bone.bone.hide = False
                    
                    else:
                        for target in variable.targets:
                            if not target.id or target.id.type != 'ARMATURE':
                                continue

                            target_bone = target.id.pose.bones.get(target.bone_target)                         
                            if target_bone:
                                target_bone.bone.hide = False



        return {'FINISHED'}


class RIGTB_MT_mix_weight_menu(bpy.types.Menu):
    bl_label = "Mix Weight Menu"

    def draw(self, context):
        layout = self.layout

        layout.operator('rigtb.mix_weight_from_clipboard', text='Add').mode = 'ADD'
        layout.operator('rigtb.mix_weight_from_clipboard', text='Substract').mode = 'SUBSTRACT'
        layout.operator('rigtb.mix_weight_from_clipboard', text='Multiply').mode = 'MULTIPLY'
        layout.operator('rigtb.mix_weight_from_clipboard', text='Multiply by Inverse').mode = 'MULTIPLY_BY_INVERSE'


def draw_vertex_group_menu(self, context):
    self.layout.separator()

    self.layout.operator('rigtb.copy_weight_to_clipboard', text='Copy Weight')
    self.layout.menu('RIGTB_MT_mix_weight_menu', text='Mix Weight')
    

    #self.layout.operator('rigtb.mix_weight_from_clipboard')
    

classes = (
    RIGTB_MT_mix_weight_menu,
    RIGTB_OT_symmetrize_drivers,
    RIGTB_OT_mix_weight_from_clipboard,
    RIGTB_OT_copy_weight_to_clipboard,
    RIGTB_OT_reset_stretch,
    RIGTB_OT_copy_bones_to_clipboard,
    RIGTB_OT_paste_bones_from_clipboard,
    RIGTB_OT_set_deform_bones,
    RIGTB_OT_clear_invalid_drivers,
    RIGTB_OT_set_ik_pole_angles,
    RIGTB_OT_rename_bone,
    RIGTB_OT_hide_bones,
    RIGTB_OT_isolate_bone_hierarchy,
    RIGTB_OT_symmetrize_bones,
    RIGTB_OT_set_bones_euler
)

def register():
    for c in classes:
        bpy.utils.register_class(c)
    
    bpy.types.MESH_MT_vertex_group_context_menu.append(draw_vertex_group_menu)

def unregister():
    bpy.types.MESH_MT_vertex_group_context_menu.remove(draw_vertex_group_menu)

    for c in reversed(classes) :
        bpy.utils.unregister_class(c)
    