bl_info = {
    "name": "Rigging Toolbox",
    "author": "Christophe Seux",
    "version": (1, 0),
    "blender": (3, 4, 1),
    "location": "",
    "description": "",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Rigging"}


# Ensure the name of the module in python import
import sys
from pathlib import Path
import importlib

module_name = Path(__file__).parent.name
sys.modules.update({'rigging_toolbox': importlib.import_module(module_name)})


from rigging_toolbox import (bone_match, math_utils, rig_utils, operators, panels) #snapping_chain
from rigging_toolbox import snapping_chain

modules = (
    bone_match,
    operators,
    panels,
    snapping_chain
)

if "bpy" in locals():
    import importlib

    for mod in modules:
        importlib.reload(mod)

    importlib.reload(math_utils)
    importlib.reload(rig_utils)

import bpy


def register():
    print('Register Riggin Toolbox')
    for mod in modules:
        mod.register()

def unregister():
    for mod in reversed(modules):
        mod.unregister()