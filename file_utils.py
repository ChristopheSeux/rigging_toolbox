import importlib
import re
import subprocess
import platform
import sys
import unicodedata
from pathlib import Path


def import_module_from_path(path):
    from importlib import util

    try:
        path = Path(path)
        spec = util.spec_from_file_location(path.stem, str(path))
        mod = util.module_from_spec(spec)
        
        spec.loader.exec_module(mod)

        return mod

    except Exception as e:
        print(f'Cannot import file {path}')
        print(e)

def norm_str(string, separator='_', format=str.lower, padding=0):    
    string = str(string)
    string = string.replace('_', ' ')
    string = string.replace('-', ' ')
    string = re.sub('[ ]+', ' ', string)
    string = re.sub('[ ]+\/[ ]+', '/', string)
    string = string.strip()

    if format:
        string = format(string)
    
    # Padd rightest number
    string = re.sub(r'(\d+)(?!.*\d)', lambda x : x.group(1).zfill(padding), string)
    
    string = string.replace(' ', separator)
    string = unicodedata.normalize('NFKD', string).encode('ASCII', 'ignore').decode("utf-8")

    return string