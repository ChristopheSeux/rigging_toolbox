#from rigging_toolbox.snapping_chain import functions
from rigging_toolbox.snapping_chain import properties, ui, operators

modules = (
    operators,
    properties,
    ui,
)

modules = ()

if "bpy" in locals():
    import importlib

    for mod in modules:
        importlib.reload(mod)

import bpy

def register():
    for mod in modules:
        mod.register()

def unregister():
    for mod in reversed(modules):
        mod.unregister()
