
import bpy
from bpy.types import UIList


class RIGTB_PT_UL_snapping_chain(UIList):
    def draw_item(self, context, layout, data, item, icon, active_data,
                  active_propname, index):

        layout.prop(item, "name", icon='CON_SPLINEIK', emboss=False, text='')


class RIGTB_PT_snapping_chain(bpy.types.Panel):
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "data"
    bl_label = "Snapping Chain"
    bl_options = {'DEFAULT_CLOSED'}

    @classmethod
    def poll(cls, context):
        return (context.object and context.object.type =='ARMATURE')

    def draw(self, context) :
        layout = self.layout
        col = layout.column(align=True)


        ob = context.object
        armature = ob.data
        snapping_chain = armature.snapping_chain
        index = snapping_chain.chain_index

        row = col.row()

        row.template_list("RIGTB_PT_UL_snapping_chain", "", snapping_chain, "chains", snapping_chain, "chain_index")

        col = row.column(align=True)

        op = col.operator('rigging_toolbox.add_item', icon='ADD', text='')
        op.collection = repr(snapping_chain.chains)
        op.name = 'Chain'


        op = col.operator('rigging_toolbox.remove_item', icon='REMOVE', text='')
        op.collection = repr(snapping_chain.chains)
        op.index = index

        #col.separator()
        #col.menu("RIGTB_MT_snapping_chain", icon="DOWNARROW_HLT", text="")
        #col.separator()

        op = col.operator("rigging_toolbox.move_item", icon='TRIA_UP', text="")
        op.collection = repr(snapping_chain.chains)
        op.index_prop_name = 'chain_index'
        op.direction = 'UP'

        op = col.operator("rigging_toolbox.move_item", icon='TRIA_DOWN', text="")
        op.direction = 'DOWN'


classes = (
    RIGTB_PT_UL_snapping_chain,
    RIGTB_PT_snapping_chain
)

register, unregister = bpy.utils.register_classes_factory(classes)