

import bpy
from bpy.types import Operator
from bpy.props import StringProperty, IntProperty, EnumProperty


class RIGTB_OT_add_item(Operator):
    bl_idname = "rigging_toolbox.add_item"
    bl_label = "Add a collection item"
    bl_options = {'REGISTER', 'UNDO'}

    collection : StringProperty()
    name : StringProperty()
    attrs : StringProperty()

    def execute(self, context):
        collection = eval(self.collection)

        item = collection.add()
        item.name = self.name
        #for k, v in eval(self.attrs):
        #    setattr(item, k, v)

        return {'FINISHED'}


class RIGTB_OT_remove_item(Operator):
    bl_idname = "rigging_toolbox.remove_item"
    bl_label = "Remove collection item"
    bl_options = {'REGISTER', 'UNDO'}

    collection : StringProperty()
    index : IntProperty()

    def execute(self, context):
        collection = eval(self.collection)
        collection.remove(self.index)

        return {'FINISHED'}


class RIGTB_OT_move_item(Operator):
    bl_idname = "rigging_toolbox.move_item"
    bl_label = "Move item in List"
    bl_description = "Move the active item the list"
    bl_options = {'UNDO', 'REGISTER'}

    collection : StringProperty()
    index_collection : StringProperty()
    index_prop_name : StringProperty(default='index', options={'SKIP_SAVE'})

    direction : EnumProperty(
        name="Move Direction",
        description="Direction to move the active Selection Set: UP (default) or DOWN",
        items=[
            ('UP', "Up", "", -1),
            ('DOWN', "Down", "", 1),
        ],
        default='UP',
        options={'HIDDEN'},
    )

    def execute(self, context):
        collection = eval(self.collection)
        if self.index_collection:
            index_collection = eval(self.index_collection)
        else:
            index_collection = collection.data

        index = getattr(index_collection, self.index_prop_name)

        new_index = index + (-1 if self.direction == 'UP' else 1)

        if new_index < 0 or new_index >= len(collection):
            return {'FINISHED'}

        index_collection.move(index, new_index)
        setattr(index_collection, self.index_prop_name, new_index)

        return {'FINISHED'}


classes = [
    RIGTB_OT_add_item,
    RIGTB_OT_remove_item,
    RIGTB_OT_move_item
]

def register():
    for c in classes:
        bpy.utils.register_class(c)
    
def unregister():
    for c in reversed(classes):
        bpy.utils.unregister_class(c)
    

    

