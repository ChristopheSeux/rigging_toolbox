import bpy

from bpy.props import FloatProperty, BoolProperty, FloatVectorProperty
from mathutils import Matrix

from .functions.snap_ik_fk import snap_ik_fk
from .functions.data_path import split_path, find_mirror, mirror_path, add_driver
from .functions.utils import redraw_areas, bone_list, is_bones_selected, is_bones_active, get_IK_bones
from .PG_addon import ATB_PG_snapping_chain, ATB_PG_wm
from .functions.keyframe import insert_keyframe, get_transform_channel
import re


class RIGTB_OT_chain_move(bpy.types.Operator):
    bl_idname = "rigging_toolbox.chain_move"
    bl_label = "Move Chain in List"
    bl_description = "Move the active Chain up/down the list of sets"
    bl_options = {'UNDO', 'REGISTER'}

    direction: bpy.props.EnumProperty(
        name="Move Direction",
        description="Direction to move the active Selection Set: UP (default) or DOWN",
        items=[
            ('UP', "Up", "", -1),
            ('DOWN', "Down", "", 1),
        ],
        default='UP',
        options={'HIDDEN'},
    )

    def execute(self, context):
        arm = context.object.data

        ob = context.object
        armature = ob.data
        snapping_chain = armature.snapping_chain
        index = snapping_chain.IKFK_index

        new_index = index + (-1 if self.direction == 'UP' else 1)

        if new_index < 0 or new_index >= len(snapping_chain.IKFK_bones):
            return {'FINISHED'}

        snapping_chain.IKFK_bones.move(index, new_index)
        snapping_chain.IKFK_index = new_index

        return {'FINISHED'}


class RIGTB_OT_load_spaces(bpy.types.Operator):
    """Guess Spaces automaticaly"""
    bl_idname = "rigging_toolbox.load_spaces"
    bl_label = "Load Space From"

    index : bpy.props.IntProperty()
    source : bpy.props.EnumProperty(items=[(i, i, '') for i in ('Driver', 'Constraint', 'Tooltip')])

    def draw(self, context) :
        layout = self.layout
        col = layout.column(align=True)
        col.prop(self, 'source', expand=True)

    def execute(self, context):
        #print('Load Space')
        #print(self.source)

        ob = context.object
        bone_prop = ob.data.snapping_chain.space_switch_bones[self.index]
        bone = ob.pose.bones.get(bone_prop.name)

        #print(self.source)
        bone_prop.spaces.clear()

        if self.source == 'Tooltip' :
            bone_name, prop_name = split_path(bone_prop.data_path)
            space_bone = ob.pose.bones.get(bone_name)
            description = space_bone.id_properties_ui(prop_name).as_dict()['description']

            # data = re.findall( '([\w.]+)\(([0-9]+)\)', description)
            data = re.findall( '([\D]+)\(([0-9]+)\)', description)  # \D non-Digit

        elif self.source == 'Constraint' :
            armature = next((c for c in bone.constraints if c.type == 'ARMATURE'), None)
            if armature:
                var_name = 'var'
                
                data = [('None', 0)]

                add_driver(
                    source=armature, prop='influence', var_name=var_name,
                    var_object=ob, var_data_path=bone_prop.data_path,
                    expression=f'1 if {var_name} != 0 else 0'
                )

                for i, t in enumerate(armature.targets):
                    data += [(t.subtarget.title(), i+1)]

                    add_driver(
                        source=t, prop='weight', var_name=var_name,
                        var_object=ob, var_data_path=bone_prop.data_path,
                        expression=f'1 if {var_name} == {i+1} else 0'
                    )

            else:
                data = [(c.name, i) for i, c in enumerate(bone.constraints)]

        elif self.source == 'Driver' :
            drivers =  ob.animation_data.drivers
            drivers = [ d.driver for d in drivers if 
                any(t.data_path==bone_prop.data_path for t in 
                [t for v in d.driver.variables for t in v.targets]) ]
                      
            data = []
            
            for d in drivers :
                name = d.variables[0].name if d.variables else ''
                value = 0
                values =  re.findall('([0-9.]+)', d.expression)
                if values :
                    value = values[0]

                data.append((name, value))


        for name, value in data :
            s = bone_prop.spaces.add()
            s.name = re.sub(' ', '', name)
            s.value = float(value)

        redraw_areas()
        return {'FINISHED'}

    def invoke(self, context, event) :
        wm = context.window_manager
        return wm.invoke_props_dialog(self,width=150)


class RIGTB_OT_switch_space(bpy.types.Operator):
    """ Change the parent of a bone and preserve transform"""

    bl_idname = "rigging_toolbox.switch_space"
    bl_label = "Switch Space"

    index : bpy.props.IntProperty()

    #def get_items(self, context ) :
    #    ob = context.object
    #    bone_prop = ob.data.snapping_chain.space_switch_bones[self.index]
    #    return ((s.name, s.name, '') for s in bone_prop.spaces)
    #space : bpy.props.EnumProperty( items=lambda s,c : bpy.context.window_managet )

    def draw(self,context) :
        layout = self.layout
        props = context.window_manager.animtoolbox
        rig = context.object
        col = layout.column()
        col.prop(props, "space", expand=True)

    def execute(self, context):
        ob = context.object
        bone_prop = ob.data.snapping_chain.space_switch_bones[self.index]
        bone = ob.data.bones.get(bone_prop.name)
        bone_name, prop_name = split_path(bone_prop.data_path)
        space_bone = ob.pose.bones.get(bone_name)

        world_mat = space_bone.matrix.copy()

        space = context.window_manager.rigging_toolbox.space
        #print('')
        #print("space", space)
        #print([(s.name, s.value) for s in bone_prop.spaces])
        space_bone[prop_name] = next((int(s.value) for s in bone_prop.spaces if s.name==space), 0)
        #bone["space"] = [c.name for c in space_switch_bone.constraints].index(rig.data.snapping_chain.space)
        #ob.update_tag({'OBJECT'})
        #bpy.context.scene.update()
        bpy.ops.pose.visual_transform_apply()
        bpy.context.view_layer.update()

        space_bone.matrix = world_mat

        if context.scene.tool_settings.use_keyframe_insert_auto :
            ob.keyframe_insert(bone_prop.data_path)

            insert_keyframe(space_bone, custom_prop=False)
            '''
            if space_bone.rotation_mode == 'QUATERNION' :
                mode = 'rotation_quaternion'
            elif space_bone.rotation_mode == 'AXIS_ANGLE' :
                mode = 'rotation_axis_angle'
            else :
                mode = 'rotation_euler'

            ob.keyframe_insert('pose.bones["%s"].location'%(space_bone.name))
            ob.keyframe_insert('pose.bones["%s"].%s'%(space_bone.name, mode))
            ob.keyframe_insert('pose.bones["%s"].scale'%(space_bone.name))

            '''

        return {'FINISHED'}

    def invoke(self, context, event):
        ob = context.object
        bone_prop = ob.data.snapping_chain.space_switch_bones[self.index]
        ATB_PG_wm.space = bpy.props.EnumProperty(items=[(s.name, s.name, '') for s in bone_prop.spaces])

        wm = context.window_manager
        return wm.invoke_props_dialog(self, width=150)


class RIGTB_OT_copy_chains(bpy.types.Operator):
    """Copy Chains data in the clipboard"""

    bl_idname = "rigging_toolbox.copy_chains"
    bl_label = "Copy Chain"

    def execute(self, context):
        ob = context.object
        snapping_chain = ob.data.snapping_chain

        data = {'chains': [], 'space_switchs': []}

        #delete_layer
        for chain in snapping_chain.IKFK_bones :
            chain_infos = {}
            for attr in [a.identifier for a in chain.bl_rna.properties if a.identifier!='rna_type'] :
                value = getattr(chain,attr)
                if isinstance(value, bpy.types.bpy_prop_collection) :
                    sub_value = []
                    for sub_chain in value :
                        sub_chain_infos = {}
                        for sub_attr in [a.identifier for a in sub_chain.bl_rna.properties if a.identifier!='rna_type'] :
                            sub_chain_infos[sub_attr] = getattr(sub_chain,sub_attr)
                        sub_value.append(sub_chain_infos)

                    value = sub_value

                chain_infos[attr] = value
            data['chains'].append(chain_infos)

        for space_switch in snapping_chain.space_switch_bones :
            space_infos = {}
            space_infos['data_path'] = space_switch.data_path
            space_infos['name'] = space_switch.name

            space_infos['spaces'] = []
            for space in space_switch.spaces :
                space_infos['spaces'].append({'name': space.name, 'value': space.value})

            data['space_switchs'].append(space_infos)

        print(data)

        context.window_manager.clipboard = str(data)
        return {'FINISHED'}


class RIGTB_OT_paste_chains(bpy.types.Operator):

    bl_idname = "rigging_toolbox.paste_chains"
    bl_label = "Copy Chain"

    def execute(self, context):
        ob = context.object
        snapping_chain = ob.data.snapping_chain

        data = eval(context.window_manager.clipboard)

        snapping_chain.IKFK_bones.clear()
        snapping_chain.space_switch_bones.clear()

        #try :
        for chain_info in data['chains'] :
            chain = snapping_chain.IKFK_bones.add()
            for attr,value in chain_info.items() :
                #if not chain.is_property_read_only(attr) :
                if isinstance(value,list) :
                    for sub_chain in value :
                        new_sub_chain = getattr(chain,attr).add()
                        for sub_attr,sub_value in sub_chain.items() :
                            setattr(new_sub_chain,sub_attr,sub_value)

                else :
                    setattr(chain,attr,value)

        for space_infos in data['space_switchs'] :
            space_switch = snapping_chain.space_switch_bones.add()
            space_switch.data_path = space_infos['data_path']
            space_switch.name = space_infos['name']
            for space_info in space_infos['spaces'] :
                space = space_switch.spaces.add()
                space.name = space_info['name']
                space.value = space_info['value']


        #except :
        #    self.report({'ERROR'},"Wrong ClipBoard")

        return {'FINISHED'}



class RIGTB_OT_elbow_snapping(bpy.types.Operator):
    bl_idname = "rigging_toolbox.elbow_snapping"
    bl_label = "Elbow snapping"

    index : bpy.props.IntProperty(default=-1)
    name : bpy.props.StringProperty()

    def execute(self,context) :
        ob = context.object
        armature = ob.data
        snapping_chain = armature.snapping_chain

        if self.index !=-1:
            IKFK_chain = snapping_chain.IKFK_bones[self.index]
        elif self.name:
            IKFK_chain = snapping_chain.IKFK_bones[self.name]

        pin_elbow = ob.pose.bones.get(IKFK_chain.pin_elbow)
        target_elbow = ob.pose.bones.get(IKFK_chain.target_elbow)

        pin_elbow.matrix = target_elbow.matrix

        if context.scene.tool_settings.use_keyframe_insert_auto :
            context.object.keyframe_insert('pose.bones["%s"].location'%(pin_elbow.name))
        #elbow_switch_bone = ob.pose.bones.get(IKFK_chain.elbow_switch.split('"')[1])

        #setattr(elbow_switch_bone,)

        return {"FINISHED"}


class RIGTB_OT_mirror_chain(bpy.types.Operator):
    bl_idname = "rigging_toolbox.mirror_chain"
    bl_label = "Mirror IKFK chain"

    index : bpy.props.IntProperty()

    def execute(self,context) :
        ob = context.object
        armature = ob.data
        snapping_chain = armature.snapping_chain

        IKFK_chain = snapping_chain.IKFK_bones[self.index]

        mirrorChain = snapping_chain.IKFK_bones.add()

        mirrorfield = ['name','FK_root','FK_tip',
                            'IK_last','IK_tip','IK_pole',
                                'pin_elbow','target_elbow'
                        ]

        for prop in mirrorfield :
            mirrorprop = find_mirror(getattr(IKFK_chain,prop))

            if mirrorprop :
                setattr(mirrorChain,prop,mirrorprop)


        for fk_mid in IKFK_chain.FK_mid :
            mirror_fk_mid_name = find_mirror(fk_mid.name)
            mirror_fk_mid = mirrorChain.FK_mid.add()
            if mirror_fk_mid_name :
                setattr(mirror_fk_mid,'name',mirror_fk_mid_name)

        mirror_switch = mirror_path(IKFK_chain.switch_prop)

        if mirror_switch :
            mirrorChain.switch_prop = mirror_path(IKFK_chain.switch_prop)

        mirrorChain.invert_switch = IKFK_chain.invert_switch

        mirrorChain.expand = IKFK_chain.expand
        return {"FINISHED"}


class RIGTB_OT_auto_ik_fk_layers(bpy.types.Operator):
    """Guess the ik and fk layer from the bones"""
    bl_idname = "rigging_toolbox.auto_ikfklayers"
    bl_label = "Detect IKFK layers"

    index : bpy.props.IntProperty()

    def execute(self,context) :
        ob = context.object
        armature = ob.data
        snapping_chain = armature.snapping_chain

        IKFK_chain = snapping_chain.IKFK_bones[self.index]

        fk_layers = [i for i,l in enumerate(armature.bones[IKFK_chain.FK_tip].layers) if l]
        ik_layers = [i for i,l in enumerate(armature.bones[IKFK_chain.IK_tip].layers) if l]

        #print(IKFK_chain)
        #print(fk_layers,ik_layers)

        IKFK_chain.FK_layer = fk_layers[0]
        IKFK_chain.IK_layer = ik_layers[0]

        return {'FINISHED'}


class RIGTB_OT_ik_fk_snapping(bpy.types.Operator):
    bl_idname = "rigging_toolbox.snapping"
    bl_label = "Snape IK or FK chain"
    bl_options = {'REGISTER', 'UNDO'}

    #chain : bpy.props.StringProperty()
    index : bpy.props.IntProperty(default=-1)
    name : bpy.props.StringProperty()
    way : bpy.props.EnumProperty(items=[(i, i.title(), '') for i in ('TO_IK', 'TO_FK')])
    auto_switch : bpy.props.BoolProperty()

    def execute(self, context):

        way = self.way
        auto_switch = self.auto_switch

        ob = context.object

        snapping_chain = ob.data.snapping_chain
        p_bones = ob.pose.bones
        
        if self.index !=-1:
            chains = [snapping_chain.IKFK_bones[self.index]]
        elif self.name:
            chains = [snapping_chain.IKFK_bones[self.name]]
        else:
            chains = [s for s in snapping_chain.IKFK_bones if s.IK_last and is_bones_selected(bone_list(s))]

        if auto_switch:
            bpy.ops.pose.select_all(action='DESELECT')

        for chain in chains:
            switch_prop = chain.switch_prop

            FK_root = p_bones.get(chain.FK_root)
            FK_mid = [p_bones.get(b.name) for b in chain.FK_mid]
            FK_tip = p_bones.get(chain.FK_tip)

            IK_last = p_bones.get(chain.IK_last)
            IK_tip = p_bones.get(chain.IK_tip)
            IK_pole = p_bones.get(chain.IK_pole)

            IK_stretch_last = p_bones.get(chain.IK_stretch_last) if chain.get('IK_stretch_last') else None


            root_target = p_bones.get(chain.FK_root_target)
            mid_targets = [p_bones.get(b.target) for b in chain.FK_mid]
            tip_target = p_bones.get(chain.FK_tip_target)

            target_chain = [root_target] + mid_targets + [tip_target]

            invert = chain.invert_switch

            ik_fk_layer = (chain.FK_layer, chain.IK_layer)

            for c in IK_last.constraints :
                if c.type == 'IK':
                    ik_len = c.chain_count
                    break

            if not FK_mid :
                FK_mid = FK_tip.parent_recursive[:ik_len-1].reverse()

            if ik_len == 0 :
                IK_mid = ([IK_last]+IK_last.parent_recursive)[::-1]
            else :
                IK_mid = ([IK_last]+IK_last.parent_recursive[:ik_len-1])[::-1]

            IK_chain = IK_mid + [IK_tip]
            FK_chain = ([FK_root]+FK_mid)[-ik_len:] + [FK_tip]

            print('IK_chain', IK_chain)
            print('FK_chain', FK_chain)
            print('target_chain', target_chain)

            snap_ik_fk( 
                ob, 
                way, 
                switch_prop,
                fk_chain=FK_chain,
                ik_chain=IK_chain,
                ik_pole=IK_pole,
                target_chain=target_chain,
                invert=invert,
                ik_fk_layer=ik_fk_layer,
                auto_switch=auto_switch
            )

        return {'FINISHED'}


class RIGTB_OT_add_remove_field(bpy.types.Operator):
    bl_idname = "rigging_toolbox.add_remove_field"
    bl_label = "Add or Remove collection field"
    bl_options = {'REGISTER', 'UNDO'}

    values : bpy.props.StringProperty()

    def execute(self, context):
        values = eval(self.values)
        prop = eval(values['prop'])

        if values['add'] :
            chain = prop.add()
            if values.get('set') :
                for sub_prop,value in values['set'].items() :
                    setattr(chain,sub_prop,value)

        else :
            prop.remove(values['index'])

        return {'FINISHED'}


class RIGTB_OT_reset_ik(bpy.types.Operator) :
    bl_idname = "rigging_toolbox.reset_ik"
    bl_label = "Reset IK Bone"

    #chain : bpy.props.StringProperty()
    index : bpy.props.IntProperty(default=-1)
    name : bpy.props.StringProperty()

    def execute(self,context) :
        ob = context.object
        snapping_chain = ob.data.snapping_chain

        if self.index != -1:
            chain = snapping_chain.IKFK_bones[self.index]
        elif name:
            chain = snapping_chain.IKFK_bones[self.name]
        else:
            self.report({"ERROR"}, 'You must specify a chain name or a chain index')
            return {"CANCELLED"}

        IK_last = ob.pose.bones.get(chain.IK_last)
        IK_root,IK_mid = get_IK_bones(IK_last)
        IK_root.matrix_basis = Matrix()

        for bone in IK_mid :
            bone.matrix_basis = Matrix()


        if bpy.context.scene.tool_settings.use_keyframe_insert_auto:
            if not ob.animation_data:
                ob.animation_data_create()

            insert_keyframe(IK_root)
            for bone in IK_mid :
                insert_keyframe(bone)

        return {'FINISHED'}


class RIGTB_OT_keyframing_chain(bpy.types.Operator) :
    bl_idname = "rigging_toolbox.keyframing_chain"
    bl_label = "Keyframing Chain IK Bone"

    chain : bpy.props.StringProperty()
    mode: bpy.props.EnumProperty(items=[('ADD', 'Add', ''), ('REMOVE', 'Remove', '')], default='ADD') #

    def execute(self,context) :

        #print(self.mode)

        ob = context.object
        chain = eval(self.chain)
        IK_last = ob.pose.bones.get(chain.IK_last)
        IK_root,IK_mid = get_IK_bones(IK_last)
        IK_tip = ob.pose.bones.get(chain.IK_tip)
        IK_pole = ob.pose.bones.get(chain.IK_pole)
        FK_root = ob.pose.bones.get(chain.FK_root)
        FK_mid = [ob.pose.bones.get(b.name) for b in chain.FK_mid]
        FK_tip = ob.pose.bones.get(chain.FK_tip)
        switch = chain.switch_prop
        group = switch.split('"')[1].split("'")[0]

        if not ob.animation_data:
            ob.animation_data_create()

        for bone in [FK_root, FK_tip, IK_root, IK_tip, IK_pole] + FK_mid + IK_mid:
            if self.mode == 'ADD':
                insert_keyframe(bone, custom_prop=False)
            elif self.mode == 'REMOVE':
                for c in get_transform_channel(bone):
                    bone.keyframe_delete(c)

        if self.mode == 'ADD':
            ob.keyframe_insert(data_path=switch, group=group)
        elif self.mode == 'REMOVE':
            ob.keyframe_delete(switch)

        for area in context.screen.areas:
            area.tag_redraw()


        return {'FINISHED'}


class RIGTB_OT_bone_eyedropper(bpy.types.Operator):
    bl_idname = "rigging_toolbox.bone_eyedropper"
    bl_label = "Eye drop Bone"

    field : bpy.props.StringProperty()
    prop : bpy.props.StringProperty()

    def execute(self, context):
        prop = self.prop
        field = eval(self.field)

        ob = context.object
        armature = ob.data

        setattr(field,prop,context.active_pose_bone.name)


        return {'FINISHED'}


classes = (
    RIGTB_OT_chain_move,
    RIGTB_OT_load_spaces,
    RIGTB_OT_switch_space,
    RIGTB_OT_copy_chains,
    RIGTB_OT_paste_chains,
    RIGTB_OT_elbow_snapping,
    RIGTB_OT_mirror_chain,
    RIGTB_OT_auto_ik_fk_layers,
    RIGTB_OT_ik_fk_snapping,
    RIGTB_OT_add_remove_field,
    RIGTB_OT_reset_ik,
    RIGTB_OT_keyframing_chain,
    RIGTB_OT_bone_eyedropper,
)

register, unregister = bpy.utils.register_classes_factory(classes)