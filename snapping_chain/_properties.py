import bpy
from bpy.types import (
    Operator,
    Menu,
    Panel,
    UIList,
    PropertyGroup,
)
from bpy.props import (
    StringProperty,
    IntProperty,
    FloatProperty,
    EnumProperty,
    BoolProperty,
    CollectionProperty,
)

class RIGTB_PG_space_switch(PropertyGroup) :
    name : StringProperty()
    data_path : StringProperty()
    spaces : CollectionProperty(type=PropertyGroup)
    expand : BoolProperty(default=True)
    #space = EnumProperty(items = items_space,update= update_space)


class RIGTB_PG_bone_chain(PropertyGroup):
    fk_bone : StringProperty()
    ik_bone : StringProperty()


class RIGTB_PG_ik_fk(PropertyGroup) :
    #bone : CollectionProperty(type=bpy.props.PropertyGroup)

    expand : BoolProperty()

    #FK_root : StringProperty()
    #FK_root_use_target : BoolProperty(default=False)
    #FK_root_target : StringProperty()

    ik_chain : CollectionProperty(type=RIGTB_PG_bone_chain)
    fk_chain : CollectionProperty(type=PropertyGroup)
    Iik_pole : StringProperty()

    fk_layer : IntProperty()
    ik_layer : IntProperty()

    switch_prop : StringProperty()
    #stretch_ik_prop : StringProperty()
    #scale_ik_prop : StringProperty()
    #scale_fk_prop : StringProperty()

    invert_switch : BoolProperty(default=False, description='Invert the switch property')
    extra_settings : BoolProperty()
    

    #pin_elbow : StringProperty()
    #target_elbow : StringProperty()

    #IK_stretch_last : StringProperty()


class RIGTB_PG_snapping_chain(PropertyGroup):
    #constraints = StringProperty()
    ik_option : BoolProperty(override={'LIBRARY_OVERRIDABLE'}, options={'LIBRARY_EDITABLE'})
    ikfk_bones : CollectionProperty(type=RIGTB_PG_ik_fk)
    space_switch_bones : CollectionProperty(type=RIGTB_PG_space_switch)
    #snap_type : EnumProperty(items=[(i, title(i), "") for i in ("IK_FK", "SPACE_SWITCH")])

    space_switch_index : bpy.props.IntProperty()
    ikfk_index : bpy.props.IntProperty()


classes = (
    RIGTB_PG_space_switch,
    RIGTB_PG_bone_chain,
    RIGTB_PG_ik_fk,
    RIGTB_PG_snapping_chain
)

def register():
    for c in classes:
        bpy.utils.register_class(c)
    
    bpy.types.Armature.snapping_chain = bpy.props.PointerProperty(type=RIGTB_PG_snapping_chain)
    bpy.types.Bone.space_switchs = bpy.props.PointerProperty(type=RIGTB_PG_space_switch)

def unregister():
    del bpy.types.Bone.space_switchs
    del bpy.types.Armature.snapping_chain

    for c in reversed(classes) :
        bpy.utils.unregister_class(c)
    