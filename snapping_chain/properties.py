

import inspect

import bpy

from bpy.types import (
    Operator,
    Menu,
    Panel,
    UIList,
    PropertyGroup,
)
from bpy.props import (
    StringProperty,
    IntProperty,
    FloatProperty,
    EnumProperty,
    BoolProperty,
    CollectionProperty,
    PointerProperty
)

from rigging_toolbox.snapping_chain.constants import CHAIN_TYPES, CHAIN_TYPES_DIR
from rigging_toolbox.file_utils import import_module_from_path, norm_str


class CollectionPropertyGroup(PropertyGroup):
    def __iter__(self):
        return (v for v in self.values())
    def props(self):
        return [p for p in self.bl_rna.properties if p.identifier not in ('rna_type', 'name')]

    def keys(self):
        return [k for k in self.bl_rna.properties.keys() if k not in ('rna_type', 'name')]

    def values(self):
        return [getattr(self, k) for k in self.keys()]

    def items(self):
        return self.to_dict().items()

    def to_dict(self, use_name=True):
        if use_name:
            return {p.name: getattr(self, p.identifier) for p in self.props()}
        else:
            return {k: getattr(self, k) for k in self.keys()}


class RIGTB_PG_space_switch(PropertyGroup) :
    name : StringProperty()
    data_path : StringProperty()
    spaces : CollectionProperty(type=PropertyGroup)
    expand : BoolProperty(default=True)


class ChainTypes(CollectionPropertyGroup):
    pass


class RIGTB_PG_chain(PropertyGroup):
    expand : BoolProperty()

    #settings : PointerProperty(type=RIGTB_PG_chain_settings)
    chain_types : PointerProperty(type=ChainTypes)
    

    src_layer : IntProperty()
    target_layer : IntProperty()

    switch_prop : StringProperty()

    invert_switch : BoolProperty(default=False, description='Invert the switch property')
    #extra_settings : BoolProperty()
    

class RIGTB_PG_snapping_chain(PropertyGroup):
    #show_options : BoolProperty(override={'LIBRARY_OVERRIDABLE'}, options={'LIBRARY_EDITABLE'})
    chains : CollectionProperty(type=RIGTB_PG_chain)
    space_switch_bones : CollectionProperty(type=RIGTB_PG_space_switch)

    space_switch_index : bpy.props.IntProperty()
    chain_index : bpy.props.IntProperty()


def load_chain_types():
    print('Load Chain Types')

    from rigging_toolbox.snapping_chain.chain_types.chain_type import ChainType

    CHAIN_TYPES.clear()
    chain_type_files = list(CHAIN_TYPES_DIR.glob('*.py'))

    for chain_type_file in chain_type_files:
        if chain_type_file.stem.startswith('_'):
            continue

        print()

        mod = import_module_from_path(chain_type_file)
        for name, obj in inspect.getmembers(mod):

            if not inspect.isclass(obj): 
                continue

            if not ChainType in obj.__mro__:
                continue

            if obj is ChainType or name in (a.__name__ for a in CHAIN_TYPES): 
                continue

            try:
                print(f'Register Chain Type {name}')
                bpy.utils.register_class(obj)

                setattr(ChainTypes, norm_str(name), PointerProperty(type=obj))
                CHAIN_TYPES.append(obj)

            except Exception as e:
                print(f'Could not register Chain Type {name}')
                print(e)

classes = [
    RIGTB_PG_space_switch,
    ChainTypes,
    RIGTB_PG_chain,
    RIGTB_PG_snapping_chain
]


def register():
    for c in classes:
        bpy.utils.register_class(c)
    
    bpy.types.Armature.snapping_chain = bpy.props.PointerProperty(type=RIGTB_PG_snapping_chain)
    bpy.types.Bone.space_switchs = bpy.props.PointerProperty(type=RIGTB_PG_space_switch)

    load_chain_types()

def unregister():
    del bpy.types.Bone.space_switchs
    del bpy.types.Armature.snapping_chain

    for c in reversed(classes + CHAIN_TYPES):
        bpy.utils.unregister_class(c)
    
    CHAIN_TYPES.clear()
    