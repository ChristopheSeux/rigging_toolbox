
import bpy
from bpy.types import UIList

#from .functions.utils import add_row


def bone_field(layout, ob, chain, prop, text=None):

    row = add_row(layout, text=text, align=True)
    row.prop_search(chain,prop,ob.pose,'bones', text='')
    eyedrop = row.operator("rigging_toolbox.bone_eyedropper", text='', icon='EYEDROPPER')
    eyedrop.field = repr(chain)
    eyedrop.prop = prop


class RIGTB_MT_snapping_chain(bpy.types.Menu):
    """Snapping Chain Special Menu"""
    bl_label = "Snapping Chain Special"

    def draw(self, context):
        layout = self.layout

        return

        ob = context.object
        armature = ob.data
        snapping_chain = armature.snapping_chain
        index = snapping_chain.chain_index

        if index == -1 or index > len(snapping_chain.bones):
            return

        chain = snapping_chain.bones[index]

        mirror_chain = layout.operator('rigging_toolbox.mirror_chain', icon='MOD_MIRROR', text='Mirror Chain').index=index

        op_remove = layout.operator('rigging_toolbox.add_remove_field', icon='ADD', text='Add FK Bone')
        op_remove.values = str({'add': True, 'prop': repr(chain.FK_mid)})

        op_add = layout.operator('rigging_toolbox.add_remove_field', icon='REMOVE', text='Remove FK Bone')
        op_add.values = str({'add': False, 'prop': repr(chain.FK_mid),'index': len(chain.FK_mid)-1})

        layout.operator("rigging_toolbox.copy_chains", text='Copy Chains', icon="COPYDOWN")
        layout.operator("rigging_toolbox.paste_chains", text='Paste Chains', icon="PASTEDOWN")


class RIGTB_PT_UL_snapping_chain(UIList):
    def draw_item(self, context, layout, data, item, icon, active_data,
                  active_propname, index):

        layout.prop(item, "name", icon='CON_SPLINEIK', emboss=False, text='')



class RIGTB_PT_snapping_chain_bones(bpy.types.Panel):
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "data"
    bl_label = "Bones"
    bl_parent_id = 'RIGTB_PT_snapping_chain'
    bl_options = {'DEFAULT_CLOSED'}

    @classmethod
    def poll(cls, context):
        armature = context.object.data
        snapping_chain = armature.snapping_chain
        index = snapping_chain.chain_index

        if index == -1 or index > len(snapping_chain.bones):
            return False

        return True

    def draw(self, context) :
        layout = self.layout
        col = layout.column(align=False)

        return

        ob = context.object
        armature = ob.data
        snapping_chain = armature.snapping_chain
        index = snapping_chain.chain_index
        chain = snapping_chain.bones[index]

        bone_field(col, ob, chain, 'FK_root', text='FK Root')

        for j,bone in enumerate(chain.FK_mid) :
            bone_field(col, ob,bone, 'name', text='FK Mid %02d'%(j+1))

        bone_field(col, ob, chain, 'FK_tip', text='FK Tip')

        col.separator()
        bone_field(col, ob, chain, 'IK_last', text='IK Last')

        '''
        for bone in chain.IK_mid :
            bone_field(col,ob,bone,'name')
            '''

        bone_field(col, ob, chain, 'IK_tip', text='IK Tip')
        bone_field(col, ob, chain, 'IK_pole', text='IK Pole')

        col.separator()

        row = add_row(col, text='Layers')
        row.operator('rigging_toolbox.auto_ikfklayers',text='', icon='SHADERFX').index = index
        row.prop(chain,'FK_layer', text='FK_layer')
        row.prop(chain,'IK_layer',text='IK_layer')




class RIGTB_PT_snapping_chain_properties(bpy.types.Panel):
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "data"
    bl_label = "Properties"
    bl_parent_id = 'RIGTB_PT_snapping_chain'
    bl_options = {'DEFAULT_CLOSED'}

    @classmethod
    def poll(cls, context):
        armature = context.object.data
        snapping_chain = armature.snapping_chain
        index = snapping_chain.chain_index

        chain = snapping_chain.bones[index]

        if index == -1 or index > len(snapping_chain.bones):
            return False

        return True

    def draw(self, context) :
        layout = self.layout
        col = layout.column(align=False)

        return

        ob = context.object
        armature = ob.data
        snapping_chain = armature.snapping_chain
        index = snapping_chain.chain_index
        chain = snapping_chain.bones[index]

        row = add_row(col, text='Switch')
        row.prop(chain, 'switch_prop', text='')
        row.separator()
        row.prop(chain, 'invert_switch',text='')

        row = add_row(col, text='Stretch IK')
        row.prop(chain, 'stretch_ik_prop', text='')

        row = add_row(col, text='Scale FK')
        row.prop(chain,'scale_fk_prop', text='')

        row = add_row(col, text='Scale IK')
        row.prop(chain,'scale_ik_prop', text='')


class RIGTB_PT_snapping_targets(bpy.types.Panel):
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "data"
    bl_label = "Targets"
    bl_parent_id = 'RIGTB_PT_snapping_chain'
    bl_options = {'DEFAULT_CLOSED'}

    @classmethod
    def poll(cls, context):
        armature = context.object.data
        snapping_chain = armature.snapping_chain
        index = snapping_chain.chain_index

        if index == -1 or index > len(snapping_chain.bones):
            return False

        return True

    def draw(self, context) :
        layout = self.layout
        col = layout.column(align=False)

        return

        ob = context.object
        armature = ob.data
        snapping_chain = armature.snapping_chain
        index = snapping_chain.chain_index
        chain = snapping_chain.bones[index]


        bone_field(col, ob, chain, 'FK_root_target', text='FK Root')

        for j,bone in enumerate(chain.FK_mid) :
            bone_field(col, ob, bone, 'target', text='FK Mid %02d'%(j+1))

        bone_field(col, ob, chain, 'FK_tip_target', text='FK Tip')


class RIGTB_PT_snapping_chain_extra_settings(bpy.types.Panel):
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "data"
    bl_label = "Extra Settings"
    bl_parent_id = 'RIGTB_PT_snapping_chain'
    bl_options = {'DEFAULT_CLOSED'}

    @classmethod
    def poll(cls, context):
        armature = context.object.data
        snapping_chain = armature.snapping_chain
        index = snapping_chain.chain_index

        if index == -1 or index > len(snapping_chain.bones):
            return False

        return True

    def draw(self, context) :
        layout = self.layout
        col = layout.column(align=False)

        return

        ob = context.object
        armature = ob.data
        snapping_chain = armature.snapping_chain
        index = snapping_chain.chain_index
        chain = snapping_chain.bones[index]


        row = add_row(col, text='IK Stretch Last')
        row.prop_search(chain, 'IK_stretch_last', ob.pose, 'bones', text='')
        eyedrop = row.operator('rigging_toolbox.bone_eyedropper', text='', icon='EYEDROPPER')
        eyedrop.field = repr(chain)
        eyedrop.prop = 'IK_stretch_last'

        row = add_row(col, text='Pin Elbow')
        row.prop_search(chain, 'pin_elbow', ob.pose, 'bones', text='')
        eyedrop = row.operator('rigging_toolbox.bone_eyedropper', text='', icon='EYEDROPPER')
        eyedrop.field = repr(chain)
        eyedrop.prop = 'pin_elbow'

        row = add_row(col, text='Target Elbow')
        row.prop_search(chain,'target_elbow', ob.pose, 'bones', text='')
        eyedrop = row.operator('rigging_toolbox.bone_eyedropper', text='', icon='EYEDROPPER')
        eyedrop.field = repr(chain)
        eyedrop.prop = 'target_elbow'



class RIGTB_PT_snapping_chain(bpy.types.Panel):
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "data"
    bl_label = "Snapping Chain"
    #bl_parent_id = 'RIGTB_PT_snapping_chain'
    bl_options = {'DEFAULT_CLOSED'}

    @classmethod
    def poll(cls, context):
        return (context.object and context.object.type =='ARMATURE')

    def draw(self, context) :
        layout = self.layout
        col = layout.column(align=True)


        ob = context.object
        armature = ob.data
        snapping_chain = armature.snapping_chain
        index = snapping_chain.chain_index

        row = col.row()

        row.template_list("RIGTB_PT_UL_snapping_chain", "", snapping_chain, "bones", snapping_chain, "chain_index")

        col = row.column(align=True)

        add_op = col.operator('rigging_toolbox.add_remove_field', icon='ADD', text='')
        add_op.values = str({'add':True, 'prop':repr(snapping_chain.bones)})

        remove_op = col.operator('rigging_toolbox.add_remove_field', icon='REMOVE', text='')
        remove_op.values = str({'add':False, 'prop':repr(snapping_chain.bones), 'index':index})

        col.separator()
        col.menu("RIGTB_MT_snapping_chain", icon="DOWNARROW_HLT", text="")
        col.separator()

        col.operator("rigging_toolbox.chain_move", icon='TRIA_UP', text="").direction = 'UP'
        col.operator("rigging_toolbox.chain_move", icon='TRIA_DOWN', text="").direction = 'DOWN'






classes = (
    RIGTB_MT_snapping_chain,
    RIGTB_PT_UL_snapping_chain,
    RIGTB_PT_snapping_chain,
    RIGTB_PT_snapping_chain_bones,
    RIGTB_PT_snapping_targets,
    RIGTB_PT_snapping_chain_properties,
    RIGTB_PT_snapping_chain_extra_settings,
)

register, unregister = bpy.utils.register_classes_factory(classes)