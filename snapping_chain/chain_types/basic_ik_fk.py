
from rigging_toolbox.snapping_chain.chain_types.chain_type import ChainType
from bpy.types import PropertyGroup
from bpy.props import CollectionProperty, StringProperty, IntProperty, BoolProperty


class BasicIKFK(ChainType):
    ik_bones : CollectionProperty(type=PropertyGroup)
    fk_bones : CollectionProperty(type=PropertyGroup)
    
    ik_pole : StringProperty()

    fk_layer : IntProperty()
    ik_layer : IntProperty()

    switch_prop : StringProperty()

    invert_switch : BoolProperty(default=False, description='Invert the switch property')
    extra_settings : BoolProperty()