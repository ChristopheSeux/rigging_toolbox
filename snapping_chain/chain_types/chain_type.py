

import bpy
from bpy.types import PropertyGroup
from bpy.props import PointerProperty, StringProperty, BoolProperty


class ChainType(PropertyGroup):
    expand : BoolProperty()
    