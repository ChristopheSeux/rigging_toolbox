import string
import sys
import re

import bpy
from mathutils import Vector, Matrix

from rigging_toolbox.math_utils import round_vector



def split_path(data_path):
    return re.findall(r'\[\"(.*?)\"\]', data_path)

def is_left_side(name):

    name = re.sub(r'\.[0-9]{3}$', '', name)

    names = [name]
    separators = (".", "_", "-")

    splitted_path = split_path(name)
    if splitted_path:
        names = splitted_path
    
    #print(names)

    for name in names:
        name = name.lower()
        for sep in separators:
            if name.startswith('l' + sep) or name.endswith(sep + 'l'):
                return True

            if name.startswith('r' + sep) or name.endswith(sep + 'r'):
                return False

def get_bones_by_side(rig, side="LEFT"):
    bones = []
    
    threshold = 0.000001
    for bone in rig.data.bones:
        if side == 'RIGHT' and (bone.head_local.x < -threshold or bone.tail_local.x < -threshold) and is_left_side(bone.name) is False:
            bones.append(bone)
    
        elif side == 'LEFT' and (bone.head_local.x > -threshold or bone.tail_local.x > -threshold) and is_left_side(bone.name):
            bones.append(bone)

        elif side == 'CENTER' and (abs(bone.head_local.x) < threshold and abs(bone.tail_local.x) < threshold) and is_left_side(bone.name) is None:
            bones.append(bone)

    return bones

def flip_name(name):
    """return string flipped name or None if not symetric name"""
    prefix = ''
    replace = ''
    number = ''
    index = None
    is_set = False
    flip_name = None
    separators = (".", "_", "-")
    side_data = {"R": "L", "r": "l", "L": "R", "l": "r"}

    # Always copy the name, since this can be called with an uninitialized string
    len_name = len(name)
    if len_name < 3:
        # We don't do names like .R or .L
        return

    # We first check the case with a .### extension, let's find the last period
    if name[- 1].isdigit():
        index = name.rfind('.')
        all_digit =True
        for string in [s for s in name[index+1:] if not s.isdigit()]:
            all_digit=False
        if index != -1 and all_digit: 
            number = name[index:]
            name = name[:index]
            len_name = len(name)
     
    # First case; separator (`.` or `_`) with extensions in `r R l L`.
    if len_name > 3 and name[- 2] in separators:
        for letter, side in side_data.items():
            if name[- 1] == letter:
                prefix = name[:- 1]
                replace = side
                flip_name = prefix + replace + number
                is_set = True
                break

    # case; beginning with r R l L, with separator after it
    if not is_set and name[1] in separators:
        for letter, side in side_data.items():
            if name[0] == letter:
                prefix = name[1:]
                replace = side
                is_set = True
                flip_name = replace + prefix + number
                break

    return flip_name

def flip_data_path(data_path):
    
    new_data_path = None
    splitted_path = split_path(data_path)
    flipped_parts = []

    for path_part in splitted_path:
        if flip_name(path_part):      
            flipped_parts.append(flip_name(path_part))
            new_data_path = data_path#init variable    

    for path_part, flipped_part in zip(splitted_path, flipped_parts):
        new_data_path = new_data_path.replace(path_part, flipped_part)

    return new_data_path

def symmetrize_driver(fcurve):
    ob = fcurve.id_data
    flipped_data_path = flip_data_path(fcurve.data_path)

    try:
        ob.path_resolve(fcurve.data_path)
    except ValueError:
        print(f"Driver {fcurve} is not valid")
        return
    
    try:
        ob.path_resolve(flipped_data_path)
    except ValueError:
        print(f"Data Path {flipped_data_path} not exist")
        return

    flipped_fcurve = ob.animation_data.drivers.find(flipped_data_path, index=fcurve.array_index)
    if flipped_fcurve:
        ob.animation_data.drivers.remove(flipped_fcurve)

    try:
        new_fcurve = ob.driver_add(flipped_data_path, fcurve.array_index)
    except TypeError:
        # For non array value index must be -1
        new_fcurve = ob.driver_add(flipped_data_path, -1)

    new_fcurve.driver.expression = fcurve.driver.expression
    new_fcurve.driver.type = fcurve.driver.type
    new_fcurve.driver.use_self = fcurve.driver.use_self

    for var in fcurve.driver.variables:
        symmetrize_variable(var, new_fcurve.driver)
                
def symmetrize_variable(var, driver):

    new_var = driver.variables.new()
    new_var.type = var.type
    new_var.name = var.name
    
    for i, target in enumerate(var.targets):
        new_target = new_var.targets[i]

        new_target.id = target.id
        if target.id is not None and target.id_type == "OBJECT" and flip_name(target.id.name):
            flip_ob = bpy.data.objects.get(flip_name(target.id.name))

            if flip_ob:
                new_target.id = flip_ob
        
        new_target.data_path = flip_data_path(target.data_path) or target.data_path
        new_target.bone_target = flip_name(target.bone_target) or target.bone_target
        new_target.rotation_mode = target.rotation_mode
        new_target.transform_space = target.transform_space
        new_target.transform_type = target.transform_type                                     

'''
def symetrize_drivers(ob):
    """add missing symetric driver """
    if ob.animation_data and ob.animation_data.drivers:
        for fcurve in ob.animation_data.drivers:
            flipped_path = flip_data_path(o, fcurve.data_path)
            if flipped_path:
                sym_driver = False
                for driver_sym_check in [ d for d in o.animation_data.drivers if d.data_path==flipped_path and d.array_index==fcurve.array_index]:
                    sym_driver=True
                    break
                if not sym_driver :
                    print(f"create driver: {flipped_path} on index:{fcurve.array_index}")
                    symmetrize_driver(o, fcurve,  flipped_path)
    '''

def signed_angle(vector_u, vector_v, normal):
    # Normal specifies orientation
    angle = vector_u.angle(vector_v)
    if vector_u.cross(vector_v).angle(normal) < 1:
        angle = -angle
    return angle

def set_pole_angle(bone):
    
    ik_bone = bone.bone
    ik_constraint = next((c for c in bone.constraints if c.type=='IK'), None)

    if not ik_constraint:
        return
    
    if not ik_constraint.pole_subtarget:
        return

    pole_bone = ik_constraint.pole_target.data.bones[ik_constraint.pole_subtarget]
    pole_location = pole_bone.head_local
    

    base_bone = ik_bone
    for i in range(ik_constraint.chain_count or 100):
        if not base_bone.parent:
            break
        
        base_bone = base_bone.parent   
    
    x_axis = base_bone.x_axis @ base_bone.matrix
    pole_normal = (ik_bone.tail_local - base_bone.head_local).cross(pole_location - base_bone.head_local)
    projected_pole_axis = pole_normal.cross(base_bone.tail_local - base_bone.head_local)
    pole_angle = signed_angle(x_axis, projected_pole_axis, base_bone.tail_local - base_bone.head_local)
    
    ik_constraint.pole_angle = pole_angle
    
def get_selected_bones(rig, mode=None):
    if rig.mode == 'POSE':
        bones = [b.bone for b in bpy.context.selected_pose_bones]
    elif rig.mode == 'EDIT':
        bones = bpy.context.selected_bones
    else:
        bones = [b for b in rig.data.bones if b.select]
    
    # Filter the bones that belong to the specified rig
    bones = [b for b in bones if b.id_data == rig.data]

    if mode == 'POSE':
        return [rig.pose.bones[b.name] for b in bones]
    elif mode == 'EDIT':
        bpy.ops.object.mode_set(mode='EDIT')
        return [rig.data.edit_bones[b.name] for b in bones]
    elif mode == 'DATA':
        return [rig.data.bones[b.name] for b in bones]
    
    return bones

def armature_info(rig) :
    #print('rig')
    if hasattr(rig, 'type') :
        rig = rig.data

    view_layer = bpy.context.view_layer
    info = {}

    active_object = view_layer.objects.active
    mode = active_object.mode

    ob = bpy.data.objects.new('metarig', rig)
    bpy.context.collection.objects.link(ob)
    ob.select_set(True)
    view_layer.objects.active = ob

    bpy.ops.object.mode_set(mode= 'OBJECT')
    bpy.context.view_layer.objects.active = ob

    bpy.ops.object.mode_set(mode= 'EDIT')

    for edit_bone in ob.data.edit_bones :
        info[edit_bone.name] ={}

        roll = round(edit_bone.roll,5)
        head = round_vector(edit_bone.head)
        tail = round_vector(edit_bone.tail)

        info[edit_bone.name]['roll'] = roll
        info[edit_bone.name]['head'] = head
        info[edit_bone.name]['tail'] = tail
        info[edit_bone.name]['vector'] = tail-head
        info[edit_bone.name]['length'] = edit_bone.length

    #restore active object
    bpy.ops.object.mode_set(mode= 'OBJECT')
    bpy.data.objects.remove(ob, do_unlink=True)

    bpy.context.view_layer.objects.active = active_object
    bpy.ops.object.mode_set(mode= mode)

    return info

def get_bone_datas(bones):
    bone_datas = {}
    for bone in bones:
        bone_datas[bone.name] = {
            'matrix': repr(bone.matrix.to_4x4()),
            'length': bone.length
            #'head': bone.head_local, 
            #'tail': bone.tail_local,
            #'roll': bone.roll
        }
    
    return bone_datas

def new_bones_from_data(rig, bone_datas):
    #print("new_bones_from_data", bone_datas)

    active_object = bpy.context.view_layer.objects.active
    mode = active_object.mode
    #print('new_bones_from_data', mode)

    bpy.ops.object.mode_set(mode='OBJECT')
    #bpy.ops.armature.select_all(action='DESELECT')
    bpy.context.view_layer.objects.active = rig
    bpy.ops.object.mode_set(mode='EDIT')

    for bone_name, bone_data in bone_datas.items():
        edit_bone = rig.data.edit_bones.new(bone_name)

        #print(edit_bone)

        edit_bone.length = bone_data['length']

        #print(bone_data)
        edit_bone.matrix = eval(bone_data['matrix'])

        #edit_bone.head = bone_data['head']
        #edit_bone.tail = bone_data['tail']
        #edit_bone.roll = bone_data['roll']

        #print(edit_bone.head, edit_bone.tail, edit_bone.roll)
    
    
    rig.data.edit_bones.update()
    bpy.context.view_layer.update()

    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.context.view_layer.objects.active = active_object
    #print('Reset Mode', mode)
    bpy.ops.object.mode_set(mode=mode)

def reset_bone_stretch(rig):
    # reset stretch_to constraints

    context = bpy.context.copy()

    for bone in rig.pose.bones :
        for constraint in bone.constraints :
            context["active_pose_bone"] = bone

            if constraint.type == "STRETCH_TO" :
                # override context by passing dic as first argument
                bpy.ops.constraint.stretchto_reset(context, constraint=constraint.name, owner='BONE')

            if constraint.type == "CHILD_OF" :
                if not constraint.target : continue

                if constraint.target.type == 'ARMATURE' :
                    bone = constraint.target.pose.bones.get(constraint.subtarget)
                    if bone :
                        constraint.inverse_matrix = bone.matrix.inverted()

def find_rig_users(rig) :
    users = []
    for ob in bpy.context.scene.objects :
        if ob.find_armature()== rig or ob.parent == rig and ob not in users:
            users.append(ob)

    return ob


