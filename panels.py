import bpy


class RIGTB_PT_rig_toolbox(bpy.types.Panel):
    bl_label = "Rigging Toolbox"
    bl_category = "Rigging"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    # bl_context = "posemode"

    @classmethod
    def poll(cls, context):
        return context.object

    def draw(self, context):
        layout = self.layout

        layout.operator("rigtb.symmetrize_drivers", icon="DRIVER", text='Symmetrize Drivers')

        if context.object.type == 'ARMATURE':

            layout.operator("rigtb.symmetrize_bones", text="Symmetrize Bones")
            layout.separator()

            layout.operator("rigtb.reset_bone_stretch", icon="CONSTRAINT_BONE", text='Reset Bone Stretch')
            layout.operator("rigtb.set_ik_pole_angles", text='Set ik pole angles', icon='CONSTRAINT_BONE')

            layout.operator("rigtb.clear_invalid_drivers", text='Clear Unvalid Drivers', icon='DRIVER')

            layout.operator("rigtb.rename_bone", text='Rename Bone Remap', icon='BONE_DATA')

            col = layout.column(align=True)
            col.operator("rigtb.copy_bones_to_clipboard", text='Copy Bone to Clipboard', icon='COPYDOWN')
            col.operator("rigtb.paste_bones_from_clipboard", text='Paste Bone to Clipboard', icon='PASTEDOWN')
            
            layout.separator()
            row = layout.row(align=True)
            row.operator("rigtb.hide_bones", text='Hide Left Bones', icon='HIDE_OFF').side = 'LEFT'
            row.operator("rigtb.hide_bones", text='Hide Right Bones', icon='HIDE_OFF').side = 'RIGHT'

            layout.operator("rigtb.isolate_bone_hierarchy", text="Isolate Bone Hierarchy")
            layout.operator("rigtb.set_deform_bones", text="Set Deform Bones")
            layout.operator("rigtb.set_bones_euler", text="Set Bones Euler")

            #row.operator("object.update_rig", icon="FILE_REFRESH", text='Update')

classes = (
    RIGTB_PT_rig_toolbox,
)

def register():
    for c in classes:
        bpy.utils.register_class(c)

def unregister():
    for c in reversed(classes) :
        bpy.utils.unregister_class(c)